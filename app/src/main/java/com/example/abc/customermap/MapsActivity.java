package com.example.abc.customermap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;

import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    public static final int LOCATION_UPDATE_MIN_DISTANCE = 10;
    public static final int LOCATION_UPDATE_MIN_TIME = 5000;
//
//    @Bind(R.id.mapview)
//    MapView mMapView;
//    @Bind(R.id.toolbar)
//    Toolbar mToolbar;
//    @Bind(R.id.fab)
//    FloatingActionButton mFab;
  static EditText text;
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
//                Log.d(String.format("%f, %f", location.getLatitude(), location.getLongitude()));
                drawMarker(location);
                mLocationManager.removeUpdates(mLocationListener);
            } else {
                Log.d("Sidd", "Location is null");
            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
    private LocationManager mLocationManager;
    private Geocoder geocoder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        initComponent();
        geocoder = new Geocoder(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
   mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        initMap();
        getCurrentLocation();

    }

    private void initComponent(){
    text=(EditText)findViewById(R.id.edt_setLocation);
        Button setLocation=(Button)findViewById(R.id.btn_current_location);
        Button btnSubmit=(Button)findViewById(R.id.btn_submit);

        setLocation.setOnClickListener(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        int googlePlayStatus = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (googlePlayStatus != ConnectionResult.SUCCESS) {
            GooglePlayServicesUtil.getErrorDialog(googlePlayStatus, this, -1).show();
            finish();
        } else {
            if (mMap != null) {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mMap.getUiSettings().setAllGesturesEnabled(true);
            }
        }

    }

    private void initMap() {
          }

    private void getCurrentLocation() {
        boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        Location location = null;
        if (!(isGPSEnabled || isNetworkEnabled)) {
//            Snackbar.make(mMapView, R.string.error_location_provider, Snackbar.LENGTH_INDEFINITE).show();
        }else{
            if (isNetworkEnabled) {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
                location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

            if (isGPSEnabled) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
                location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }
        }
        if (location != null) {
            Log.d("Sidd", String.format("getCurrentLocation(%f, %f)", location.getLatitude(),
                    location.getLongitude()));
            drawMarker(location);
//            text.setText(getAddress(new );
            List<Address> geoResult = findGeocoder(location.getLatitude(), location.getLongitude());
            if (geoResult != null) {
                List<String> geoStringResult = new ArrayList<String>();
                for (int i = 0; i < geoResult.size(); i++) {
                    Address thisAddress = geoResult.get(i);
                    String stringThisAddress = "";
                    for (int a = 0; a < thisAddress.getMaxAddressLineIndex(); a++) {
                        stringThisAddress += thisAddress.getAddressLine(a) + "\n";
                        Log.d("sidd",stringThisAddress);
                    }
                }

//            getAddress(new LatLng(location.getLatitude(),location.getLongitude()));
//            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
//            String result = null;
//            List<Address> addressList = null;
//            try {
//                addressList = geocoder.getFromLocation(
//                        location.getLatitude(),location.getLongitude(), 1);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            if (addressList != null && addressList.size() > 0) {
//                Address address = addressList.get(0);
//                for (int i=0;i<address.getMaxAddressLineIndex();i++){
//                Log.d("Sidd", String.valueOf(address.getAddressLine(i)));
//                text.setText(String.valueOf(address.getAddressLine(i)));//            Log.d("Sidd",""+);
//            }
//            }
            }


        }}
    private List<Address> findGeocoder(Double lat, Double lon){
        final int maxResults = 2;
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lon, maxResults);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        return addresses;
    }

    private void drawMarker(Location location) {
        if (mMap != null) {
            mMap.clear();
            LatLng gps = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.addMarker(new MarkerOptions()
                    .position(gps)
                    .title("Current Position"));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(gps, 12));
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapFragment.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapFragment.onLowMemory();
    }
    @Override
    protected void onResume() {
        super.onResume();
        mapFragment.onResume();
        getCurrentLocation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapFragment.onPause();
        mLocationManager.removeUpdates(mLocationListener);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_current_location:
                getCurrentLocation();
                break;
        }
    }

//    public String getLocationFromAddress(Location location){
//
//        Geocoder coder = new Geocoder(this);
//        List<Address> address;
////        GeoPoint p1 = null;
//
//
//            try {
////                address = coder.getFromLocationName(strAddress, 5);
////                if (address == null) {
////                    return null;
////                }
//                Address location = address.get(0);
//                location.get
//                location.getLatitude();
//                location.getLongitude();
//                return address.get(0).getAdminArea();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//
//
////            p1 = new GeoPoint((double) (location.getLatitude() * 1E6),
////                    (double) (location.getLongitude() * 1E6));
//
//
//return null;
//
//    }
public static void getAddressFromLocation(final double latitude, final double longitude,
                                          final Context context, final Handler handler) {
    Thread thread = new Thread() {
        @Override
        public void run() {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            String result = null;
            try {
                List<Address> addressList = geocoder.getFromLocation(
                        latitude, longitude, 1);
                if (addressList != null && addressList.size() > 0) {
                    Address address = addressList.get(0);
                    text.setText(address.getAddressLine(0));
                    StringBuilder sb = new StringBuilder();
//                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
//                        sb.append(address.getAddressLine(i)).append("\n");
//
//                    }
//                    sb.append(address.getLocality()).append("\n");
//                    sb.append(address.getPostalCode()).append("\n");
//                    sb.append(address.getCountryName());
//                    result = sb.toString();
                }
            } catch (IOException e) {
                Log.e("Sidd", "Unable connect to Geocoder", e);
//            } finally {
//                Message message = Message.obtain();
//                message.setTarget(handler);
//                if (result != null) {
//                    message.what = 1;
//                    Bundle bundle = new Bundle();
//                    result = "Latitude: " + latitude + " Longitude: " + longitude +
//                            "\n\nAddress:\n" + result;
//                    bundle.putString("address", result);
//                    message.setData(bundle);
//                } else {
//                    message.what = 1;
//                    Bundle bundle = new Bundle();
//                    result = "Latitude: " + latitude + " Longitude: " + longitude +
//                            "\n Unable to get address for this lat-long.";
//                    bundle.putString("address", result);
//                    message.setData(bundle);
//                }
//                message.sendToTarget();
            }
        }
    };
    thread.start();
}
}
